import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from "./component/home/home.component";
import {ProductComponent} from "./component/product/product.component";
import {AdminComponent} from "./component/admin/admin.component";
import {PageNotFoundComponent} from "./component/page-not-found/page-not-found.component";
import {LoginComponent} from "./component/login/login.component";
import {ViewComponent} from "./component/view/view.component";
import {AuthGuard} from "./auth-guard.guard";


const routes: Routes = [
  {path:"home",component:HomeComponent},
  {path:"product",component:ProductComponent},
  {path:"view/:id", component:ViewComponent},
  {path:"login",component:LoginComponent},
  //-------- protected route--------------
  {path:"admin",component:AdminComponent, canActivate:[AuthGuard] },
  //--------- redirect--------------------
  {path:"", redirectTo: "home", pathMatch:"full"},
  //---------- wildcard------------------
  {path:"**",component:PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
