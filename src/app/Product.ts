export  interface Product{
  id: String;
  name: String;
  qty: String;
  price: String;
}
