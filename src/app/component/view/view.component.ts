import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {

  productId!:number;
  constructor( private route : ActivatedRoute) {
    this.route.params.subscribe(
      value => {
        console.log(value);
        this.productId = value.id;
      }
    )
  }






  ngOnInit(): void {
  }

}
