import { Component, OnInit } from '@angular/core';
import {authUser, changeAuth} from "../../services/auth-service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private router:Router) { }
  logStatusName !:String;

  ngOnInit(): void {
    if(authUser){
      this.logStatusName="Logout";
    }else {
      this.logStatusName="Login";
    }
  }

  authorizedUser() {
    changeAuth();
    if(authUser){
      this.router.navigateByUrl("/admin");
    }else {
      this.router.navigateByUrl("/home");
    }
  }
}
