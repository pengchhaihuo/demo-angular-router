import { Component, OnInit } from '@angular/core';
import {Product} from "../../Product";
import {Router} from "@angular/router";

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  constructor(private route: Router) { }

  openView(pro: Product) {
    this.route.navigateByUrl("/view/"+pro.id);
  }



  ngOnInit(): void {
    this.products.push(this.pro1);
    this.products.push(this.pro2);
    this.products.push(this.pro3);
    this.products.push(this.pro4);
    this.products.push(this.pro5);
  }
  products: Product[] = [];

  //---------------------- first initialize ---------------------------------
  pro1: Product = {
    id: "1",
    name: "coca",
    qty: "10",
    price: "2000"
  }
  pro2: Product = {
    id: "2",
    name: "red bull",
    qty: "10",
    price: "2000"
  }
  pro3: Product = {
    id: "3",
    name: "Pepsi",
    qty: "10",
    price: "2000"
  }
  pro4: Product = {
    id: "4",
    name: "milo",
    qty: "10",
    price: "2000"
  }
  pro5: Product = {
    id: "5",
    name: "vigor",
    qty: "10",
    price: "2000"
  }

}
