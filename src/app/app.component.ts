import {Component} from '@angular/core';
import {changeAuth} from "./services/auth-service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'demo-app';

  constructor(private router: Router) {}

  myroute= '/view';
  id = 2;

  signIn() {
    this.router.navigateByUrl("/login")
  }
}
